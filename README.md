# Dump database

### Comand used to dump database

```bash
/usr/bin/pg_dump --host localhost --port 5432 --username "postgres" --no-password  --format plain --create --clean --verbose --file "<PATH/TO/OUTPUT/FILE>" "acedb"
```

### Restore a dump

```bash
psql acedb < ace.dump
```

### Cron + pg_dump

Create a script that dumps the DB, `nano /var/ace/ACE-DB-Dumps/sqlBackup.sh`:

```bash
#!/bin/bash
export PGPASSWORD=ace
if [ -z "$DUMPATH" ]
then
	DUMPATH="dbDumps/ace.dump"
fi
/usr/bin/pg_dump --host localhost --port 5432 --username "postgres"  --format plain --create --clean --verbose --file $DUMPATH  --dbname "acedb"
```

Then edit crontab to set cron job to do a dialy backup.

```bash
SHELL=/bin/sh
PATH=/opt/someApp/bin:/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin
HOME=/var/ace/ACE-DB-Dumps 
# pg_dump to acedb, every day, every three days, every week
00 17 * * * export DUMPATH="dbDumps/journal.dump"; /bin/bash /var/ace/ACE-DB-Dumps/sqlBackup.sh  
0 17 */3 * * export DUMPATH="dbDumps/threeDays.dump"; /bin/bash /var/ace/ACE-DB-Dumps/sqlBackup.sh  
0 17 * * 1 export DUMPATH="dbDumps/weekly.dump"; /bin/bash /var/ace/ACE-DB-Dumps/sqlBackup.sh 
```

The will do copy daily, each three days and each week on monday. If you would to do one dump every backup just remind to add a sleep before to wait postgres execution.

Obviously this is not so much professional way to do that: with huge databases the best is use more specialized software to do incremental dumps. For example check [this](https://blog.hda.me/2016/08/24/pgbackrest-for-differential-postgres-backups-alongside-with-syncthing.html]).


# Reply database

To better backup of the database, or have a system of master-slave, master-master databases systems, you can take different strategies. As I reached to understand from postgres wiki there are multiple methods and softwares that gives you different solutions. In [this](https://wiki.postgresql.org/wiki/Replication,_Clustering,_and_Connection_Pooling#Comparison_matrix) table you can see diferent third party programs that can do this. 

After v9.0 of postgres, it includes the [Streaming Replication](https://wiki.postgresql.org/wiki/Streaming_Replication) that includes different strategies to replicate asyncrhonously (by default) a database using the WAL logs. For postgres 9.6 there are also different [replication solutions](https://www.postgresql.org/docs/10/static/different-replication-solutions.html), for example [logical replication](https://www.postgresql.org/docs/10/static/logical-replication.html) that accepts multiple master servers.

Both, Streaming Replication and Logical Replication accepts sync and async replicas and are built-in postgres software.


### Master slave database system

This method conects a used database with other to have the same information at same time but only one send info to the other, so the slave can't make changes to database. We follow this [this](https://www.howtoforge.com/tutorial/how-to-set-up-master-slave-replication-for-postgresql-96-on-ubuntu-1604/) tutorial. After it we can access to acedb database. 

The problem with this method is that method can't replicate the application in other computers. To do that you will need a master-master schema.


Further reading: [this](https://wiki.postgresql.org/wiki/Streaming_Replication), and [this](https://wiki.postgresql.org/wiki/Replication,_Clustering,_and_Connection_Pooling).

# Smal description of the database

### Schemas
#### ace
Schema `ace` just has tables that only use [Augmented Cultural Experience](https://gitlab.com/CommonsVisualization/augmented-cultural-experience) software.

Starting with `a_` related to admin, login,...

`a_authority`
`a_identity`
`a_user`
`a_user_authority`

Starting with `u_` related to user content:

`u_image`
`u_image_t_taxon`
`u_image_u_itinerary_elems`
`u_itinerary`
`u_itinerary_elems`

#### sefoanco
Schema `sefoanco` just has tables that only use [Base-staff-students](https://gitlab.com/CommonsVisualization/base-staff-students) PHP frontend interface for workers and students.

It has a lot of tables. The most relevants:

| Table        | Description                                                                                         |
| ------------ | --------------------------------------------------------------------------------------------------- |
| `COMENTARI`  | Comments about state of plants, free text (current and old), user that write the comment, timestamp |
| `OBSERVACIO` |                                                                                                     |
| `EMAIL`      | Used for sending internal messages from one user to other                                           |

#### public
Schema `public` has data related to taxons that has connections from both [Augmented Cultural Experience](https://gitlab.com/CommonsVisualization/augmented-cultural-experience) and [Base-staff-students](https://gitlab.com/CommonsVisualization/base-staff-students)

It has a lot of tables. Some important:

| Table         | Description                                               |
| ------------- | ----------------------------------------------------------|
| `t_inventari` | All individual geolocalized                               |


### Read more

Issues related to improve databases structure are under label  [database](https://gitlab.com/CommonsVisualization/augmented-cultural-experience/-/issues?scope=all&utf8=%E2%9C%93&state=all&label_name[]=database) in Augmented Cultural Experience software tracker.