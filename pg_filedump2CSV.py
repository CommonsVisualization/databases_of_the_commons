# Use example:
# Recuperacio-OBSERVACIO-correcte.txt OBSERVACIO.csv 16
# or
# recuperat-COMENTARI.txt COMENTARI.csv 0,1

import sys

# To check that all " are closed
# delimOpens = ['[', ']', '(', ')', '{', '}', '"', "'"]
delimOpens = ['"', ]
# delimCloseToOpen = {']':'[', ')':'(', '}':'{', '"':'"', "'":"'"}
delimCloseToOpen = {'"':'"',}

def check_match(source):
    delimStack = ['sentinel']
    for c in source:
        if c in delimCloseToOpen and delimCloseToOpen[c] == delimStack[-1]:
            delimStack.pop()
        elif c in delimOpens:
            delimStack.append(c)
        elif c in delimCloseToOpen:
            if delimCloseToOpen[c] != delimStack.pop():
                return False
    return (len(delimStack) == 1)

if len(sys.argv) != 4:
	print('Usage:', sys.argv[0], 'input_file',  'output_file', 'PK columns')
	print('PK columns is comma separated number of columns that conform the PK, to check inconsistences')
	sys.exit()

INPUT=sys.argv[1]
OUTPUT=sys.argv[2]
# The first columns that will be PK
PKCOLUMNS=sys.argv[3]

try:
	inputfile = open(INPUT, 'r')
except FileNotFoundError:
	print('Can\'t open:', INPUT)

try:
	outputFile = open(OUTPUT, 'w')
except FileNotFoundError:
	print('Can\'t open:', OUTPUT)

linesMap = {}
inconsitences = {}
count=0
print("Parsing...")
while True:
	count +=1
	# Get next line from file
	line = inputfile.readline()

	# if line is empty
	# end of file is reached
	# Or bad standard input error
	if not line or 'Binary file (standard input) matches' in line:
		break

	# Formatting the line
	# print("{}".format(line.strip()))
	line = line.replace('COPY: ', '')
	line = line.replace('\t', '|')
	line = line.replace('\\N', '')

	finalLine = ""
	columnCount = 0

	columns = line.split('|')

	# Save pk to check for inconsitences
	pk = []
	for n in PKCOLUMNS.split(','):
		pk.append(columns[int(n)])

	for column in columns:
		columnCount += 1
		# Fix not closed quotes
		if check_match(column) is False:
			# Prevent close quote at the e3nd of new line
			if "\n" in column[-1]:
				column = column[:-1] + '"' + "\n"
			else:
				column += '"'
		# Fix bad recovered chars
		if len(column) == 1 and column.isalnum() == False:
			column = ""
		finalLine += column
		# Check if is last column
		if columnCount != len(line.split('|')):
			finalLine += '|'
		else:
			if "\n" not in finalLine[-1]:
				finalLine += '\n'

	pk = "|".join(pk)
	if pk in linesMap:
		inconsitences[pk] = [linesMap[pk], finalLine]
	linesMap[pk] = finalLine


print("Total PK Inconsistences:", len(inconsitences))
for k,v in inconsitences.items():
	print('PK:', k,)
	print(''.join([str(lst) for lst in v]))

count = 0
for k,v in linesMap.items():
	count +=1
	outputFile.write(v)
	# print("Line {} (PK: {}): {}".format(count, k, v))
print("Total lines", count)

inputfile.close()